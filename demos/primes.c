#include <omp.h>
#include <stdio.h>

int isPrime(int n) {
  if (n % 2 == 0)
    return (n == 2);
  int d = 3, d2 = 9; /* invariant: d odd, d2=d*d */
  while (d2 <= n) {
    if (n % d == 0) {
      return 0; /* n is not prime (composite) */
    }
    d++;
    d2 += 4 * d; /* because (d+2)^2=d^2 + 4*(d+1) */
    d++;
  }
  return 1; /* n is prime */
}

int main(int argc, char *argv[]) {
  int upb, cnt, task = 3;

#pragma omp parallel
  {
    int nthreads = omp_get_num_threads();

#pragma omp single
    {
      printf("Upper bound: ");
      scanf("%d", &upb);
      cnt = (upb >= 2);
    }

    int chunk = upb / (100 * nthreads);
    /* so, 100 times more tasks than threads */
    if (chunk < 0) {
      chunk = 1;
    }

    int mycnt = 0;
    int worktodo = 1; /* boolean true */
    while (worktodo) {
      /* get task */
      int mytask;
#pragma omp critical
      {
        cnt += mycnt;
        mytask = task;
        task += chunk;
      }

      int end = mytask + chunk;
      if (end > upb) {
        end = upb;
      }
      mytask += (mytask % 2 == 0);
      worktodo = mytask < end;
      if (worktodo) {
        mycnt = 0;
        for (int n = mytask; n < end; n += 2) {
          mycnt += isPrime(n);
        }
      }
    }
  } /* end parallel region */

  /* master threads prints result */
  printf("#primes in [0..%d]=%d\n", upb, cnt);

  return 0;
}
