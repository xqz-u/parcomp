#include <omp.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
  int x = 38, y = 13, z = 0;

  printf("x=%d y=%d z=%d\n\n", x, y, z);

#pragma omp parallel private(x), shared(y, z)
  for (int i = 0; i < 4; i++) {
    printf("%d: x=%d y=%d z=%d\n", i, x, y, z);
    x++;
    y++;
    z += y;
  }

  printf("\nx=%d y=%d z=%d\n\n", x, y, z);
  return 0;
}
