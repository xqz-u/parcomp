#include "IO_operations.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

void saveFrames(int bw, float **u, int width, int height,
                const char *name);

// Create an image with a 100.0 value at the top row and 0.0
// everywhere else
IMAGE createImage(int width, int height) {
  // Produce float array with
  float **plane = (float **)malloc(height * sizeof(float *));
  for (int i = 0; i < height; i++) {
    plane[i] = (float *)malloc(width * sizeof(float));
    for (int j = 0; j < width; j++) {
      if (i == 0) {
        plane[i][j]
            = 50 + 50 * sin(((double)j) * 6.0 * (M_PI / 180.0));
      } else if (j == 0 || j == height - 1) {
        plane[i][j]
            = 50 + 50 * sin(((double)i) * 6.0 * (M_PI / 180.0));
      } else {
        plane[i][j] = 0.0;
      }
    }
  }

  // saveImage(plane, width, height, "initialHeatEquation.ppm");

  IMAGE image;
  image.width = width;
  image.height = height;
  image.im = plane;

  return image;
}

IMAGE loadImage(const char *filename) {
  int height = 1;
  int width = 1;
  float **im = (float **)malloc(1 * sizeof(float *));
  IMAGE image = {width, height, im};

  return image;
}

void saveImage(float **result, int width, int height,
               const char *filename) {
  saveFrames(1, result, width, height, filename);
}

void freeImage(IMAGE image) { free(image.im); }

// Adapted from wave.c from OpenMP lab 2
void saveFrames(int bw, float **u, int width, int height,
                const char *name) {
  FILE *ppmfile;
  char filename[32];
  int i, j, k, frame, idx;
  char lut[256][3]; /* colour lookup table */
  char *rgb;        /* framebuffer */

  if (bw) {
    /* grey values */
    for (i = 0; i < 256; i++) {
      lut[i][0] = lut[i][1] = lut[i][2] = (char)i;
    }
  } else {
    /* color values */
    for (i = 0; i < 256; i++) {
      lut[i][0] = (char)i;
      lut[i][1]
          = (char)(127
                   + 2
                         * (i < 64 ? i
                                   : (i < 192 ? 128 - i : i - 255)));
      lut[i][2] = (char)(255 - i);
    }
  }

  rgb = (char *)malloc(3 * width * height * sizeof(char));

  k = 0;
  for (i = 0; i < height; i++) {
    for (j = 0; j < width; j++) {
      idx = (int)(0.0 + (255.0 / 100.0) * u[i][j]);
      rgb[k++] = (char)lut[idx][0]; /* red   */
      rgb[k++] = (char)lut[idx][1]; /* green */
      rgb[k++] = (char)lut[idx][2]; /* blue  */
    }
  }

  /* show color map (comment this out if you dont like it)
  if (N >= 255)
  {
      int i0 = N / 2 - 128;
      for (i = 0; i < 256; i++)
      {
          k = 3 * ((i + i0) * N + 10);
          for (j = 0; j < 16; j++)
          {
              rgb[k++] = lut[i][0];
              rgb[k++] = lut[i][1];
              rgb[k++] = lut[i][2];
          }
      }
  }
  */
  sprintf(filename, name);
  ppmfile = fopen(filename, "wb");
  fprintf(ppmfile, "P6\n");
  fprintf(ppmfile, "%d %d\n", width, height);
  fprintf(ppmfile, "255\n");
  fwrite(rgb, sizeof(char), 3 * width * height, ppmfile);
  fclose(ppmfile);

  free(rgb);
}
