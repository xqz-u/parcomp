/* Topic: 2D Heat Equation
*  Author: Sjoerd Bruin
*  Description: This program evaluates the heat equation on a 2D surface, and produces 
*  an output image that contains the stable solution. Computation continues until stab-
*  ility, which is defined by the difference in the heat equation being less than a 
*  certain threshold value between the previous and the new state, defined by a squared
*  Euclidean distance.
* 
*  The 2D heat equation can be iteratively computed as follows:
*      T(x, y, t+1) = T(x, y, t) + c*deltaT/h^2 * ( T(x, y-1, t) + T(x, y+1, t) + 
*                       T(x-1, y, t) + T(x+1, y, t) - 4*T(x, y, t) )
*  
*  This discrete definition has the property that the result at the next time step t+1 
*  depends only on the state of the heat equation at the current point in time t. This
*  means that we can easily accelerate this for multiple processors.

*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <omp.h>

#include "IO_operations.h"
#include "Heat_Equation_Solver.h"

int main()
{
    float tolerance = 1e-14;

    IMAGE inImage = createImage(256, 256);

    float** resultHeatEquation = solveHeatEquation(inImage, tolerance);

    saveImage(resultHeatEquation, inImage.width, inImage.height, "resultHeatEquation.ppm");
    
    free2Darray(resultHeatEquation, 256);
    free2Darray(inImage.im, 256);

    return 0;
}


