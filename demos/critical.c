#include <omp.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
  int x = 0, y = 0;

#pragma omp parallel num_threads(4)
  {
    /* every thread has a private copy */
    int myid = omp_get_thread_num();

    x++; /* unprotected shared write */

    printf("Thread %d: x=%d\n", omp_get_thread_num(), x);
  }

  printf("x=%d\n", x); /* master threads prints x */

#pragma omp parallel num_threads(4)
  {
    /* every thread has a private copy */
    int myid = omp_get_thread_num();

#pragma omp critical
    y++; /* protected shared write */

    printf("Thread %d: y=%d\n", omp_get_thread_num(), y);
  }

  printf("y=%d\n", y); /* master threads prints y */

  return 0;
}
