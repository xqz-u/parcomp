#include <float.h>
#include <math.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "Heat_Equation_Solver.h"
#include "IO_operations.h"

float **solveHeatEquation(IMAGE image, float tolerance) {
  float **currImage
      = (float **)malloc(image.height * sizeof(float *));
  float **nextImage
      = (float **)malloc(image.height * sizeof(float *));
  for (int i = 0; i < image.height; ++i) {
    currImage[i] = (float *)malloc(image.width * sizeof(float));
    nextImage[i] = (float *)malloc(image.width * sizeof(float));
  }

  // Initialise return image to input image
  for (int i = 0; i < image.height; ++i) {
    for (int j = 0; j < image.width; ++j) {
      currImage[i][j] = image.im[i][j];
      nextImage[i][j] = image.im[i][j];
    }
  }

  // Define constants
  float state_difference = FLT_MAX;

  float h = 1.0;
  float heat_constant = 2.0;
  float deltaT = (h * h) / (4.0 * heat_constant);
  float constTerm = (heat_constant * deltaT) / (h * h);

  double start = omp_get_wtime();

// Evaluate heat equation
#pragma omp parallel
  {
    float state_diff_local
        = tolerance + 1; // Initialise to something above tolerance
    while (state_diff_local > tolerance) {

      state_difference = 0.0;

// Compute new state
#pragma omp for
      for (int x = 1; x < image.height - 1; ++x) {
        for (int y = 1; y < image.width - 1; ++y) {
          nextImage[x][y]
              = currImage[x][y]
                + constTerm
                      * (currImage[x - 1][y] + currImage[x + 1][y]
                         + currImage[x][y - 1] + currImage[x][y + 1]
                         - 4 * currImage[x][y]);
        }
      }

// Compute squared Euclidean distance between old and new state
#pragma omp for reduction(+ : state_difference)
      for (int x = 1; x < image.height - 1; ++x) {
        for (int y = 1; y < image.width - 1; ++y) {
          state_difference += (currImage[x][y] - nextImage[x][y])
                              * (currImage[x][y] - nextImage[x][y]);
        }
      }

      state_diff_local = state_difference;

// Swap nextImage to currImage
// We need a single region because we want only one pointer swap, and
// we want to synchronise at the end
#pragma omp single
      {
        float **temp = currImage;
        currImage = nextImage;
        nextImage = temp;
      }

    } // End of while loop
  }   // End of omp parallel

  double duration = (omp_get_wtime() - start);

  printf("Heat equation has been solved in %lf seconds.\n",
         duration);

  free2Darray(nextImage, image.height);

  return currImage;
}

void printPlane(float **image, int width, int height) {
  for (int i = 0; i < height; ++i) {
    for (int j = 0; j < width; ++j) {
      printf("%f ", image[i][j]);
    }
    printf("\n\n");
  }
}

void free2Darray(float **arr, int height) {
  for (int i = 0; i < height; i++) {
    free(arr[i]);
  }

  free(arr);
}
