#include <omp.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
  int i;

#pragma omp for
  for (i = 0; i < 100; i++) {
    printf("A");
  }
  printf("\n");

#pragma omp parallel for
  for (i = 0; i < 100; i++) {
    printf("B");
  }
  printf("\n");

#pragma omp parallel
  {
#pragma omp for
    for (i = 0; i < 100; i++) {
      printf("C");
    }
  }
  printf("\n");

  return 0;
}
