#include <stdio.h>

int main(int argc, char *argv[]) {
  /* sequential part, executed by the master thread only */
  printf("Hello from the master thread.\n\n");

#pragma omp parallel num_threads(4)
  printf("Hello from the threads (including master)\n");

  printf("\nHere is the master again (alone)\n");

#pragma omp parallel num_threads(2)
  {
    printf("Hi again from the threads.");
    printf("This output can be quite messy.\n");
  }

  /* sequential part, executed by the master thread only */
  printf("\nGoodbye!\n");

  return 0;
}
