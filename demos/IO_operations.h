#pragma once
#ifndef IO_OPERATIONS_H

typedef struct IMAGE {
  int width;
  int height;
  float **im;
} IMAGE;

IMAGE createImage(int width, int height);
IMAGE loadImage(const char *filename);
void saveImage(float **result, int width, int height,
               const char *filename);
void freeImage(IMAGE image);

#endif
