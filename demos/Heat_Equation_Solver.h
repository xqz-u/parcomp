#pragma once
#ifndef HEAT_EQUATION_SOLVER_H
#define HEAT_EQUATION_SOLVER_H

#include <omp.h>

float** solveHeatEquation(IMAGE image, float tolerance);
void printPlane(float** image, int width, int height);

void free2Darray(float** arr, int height);

#endif
