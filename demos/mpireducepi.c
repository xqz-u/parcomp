#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
  int myrank, np, N;

  // Initialize MPI, get number of processes (in np) and rank (in
  // myrank).
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &np);
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

  // get number of samples and broadcast to all others
  if (myrank == 0) {
    printf("N=");
    fflush(stdout);
    scanf("%d", &N);
  }

  double t = MPI_Wtime();
  MPI_Bcast(&N, 1, MPI_INT, 0, MPI_COMM_WORLD);

  // Compute portion
  double x, h = 1.0 / N, sum = 0.0;
  for (int i = myrank; i < N; i += np) {
    x = h * (i + 0.5);
    sum += 1.0 / (1.0 + x * x);
  }
  sum = 4 * h * sum;

  // collect sums (reduce operator);
  double pi;
  MPI_Reduce(&sum, &pi, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

  // print answer
  if (myrank == 0) {
    // Print result.
    printf("pi = %lf\n", pi);
    printf("Elapsed time: %lf seconds\n", MPI_Wtime() - t);
  }

  // Cleanup.
  MPI_Finalize();

  return 0;
}
