// File: contrast.c
// Written by Arnold Meijster and Rob de Bruin.
// Restructured by Yannick Stoffers.
//
// A simple program for contrast stretching PPM images.

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#include "../../../lib/mpi_utils.h"
#include "image.h"

void findExtremes(int *imgChunk, int chunkSize, int *rootMin,
                  int *rootMax) {
  int min, max, i;
  min = max = imgChunk[0];
  for (i = 0; i != chunkSize; ++i) {
    min = imgChunk[i] < min ? imgChunk[i] : min;
    max = imgChunk[i] > max ? imgChunk[i] : max;
  }
  /* Collect result of individual process work into root node */
  MPI_Reduce(&min, rootMin, 1, MPI_INT, MPI_MIN, 0, MPI_COMM_WORLD);
  MPI_Reduce(&max, rootMax, 1, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);
}

void contrastStretchMPI(int low, int high, int *imgChunk,
                        int chunkSize) {
  int min, max, i;
  float scale;
  /* Determine minimum and maximum */
  findExtremes(imgChunk, chunkSize, &min, &max);
  /* Compute scale factor (only root node has collective value of
   * min & max) */
  if (IS_ROOT)
    scale = (float)(high - low) / (max - min);
  /* Communicate scale factor & minimum value to all nodes */
  MPI_Bcast(&scale, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&min, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
  /* Scale image chunks */
  for (i = 0; i != chunkSize; ++i)
    imgChunk[i] = scale * (imgChunk[i] - min);
}

/* TODO time exec time */
int main(int argc, char **argv) {
  if (argc != 3) {
    printf("Usage: %s input.pgm output.pgm\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  Image image = NULL;
  int area; // total image size

  initMPI();

  double timer = startTimer();

  // Prints current date and user. DO NOT MODIFY
  if (IS_ROOT) {
    system("date");
    system("echo $USER");
    /* master thread reads image file */
    image = readImage(argv[1]);
    area = image->width * image->height;
  }

  /* send memory location size to other threads so they can allocate
   * buffers of the proper size for data distribution */
  MPI_Bcast(&area, 1, MPI_INT, 0, MPI_COMM_WORLD);

  MemDivMPI memInfo = computePartitionsMPI(area);
  /* only master thread has image data, other process will wait for
   * data thus passing NULL works */
  int *memToSplit = image ? image->imdata[0] : NULL;
  /* scatter master data to all threads */
  int *imgChunk = distributeIntArr(memToSplit, memInfo);
  /* do work */
  contrastStretchMPI(0, 255, imgChunk, memInfo.mySize);
  /* gather results back in a single buffer */
  collectIntArr(imgChunk, memToSplit, memInfo);

  /* master thread writes image file */
  if (IS_ROOT)
    writeImage(image, argv[2]);

  /* cleanup */
  free(imgChunk);
  freeMemDivMPI(memInfo);
  if (image)
    freeImage(image);

  double end = stopTimer(timer);
  showTimeInfo(&end);

  MPI_Finalize();

  return EXIT_SUCCESS;
}
