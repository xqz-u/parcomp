#! /usr/bin/bash

# NOTE assumes existance of makefile in working directory

timestamp=$(date --iso-8601=seconds)
outfile=peregrine_$timestamp

if [ "$1" != "1" ]; then
    echo "Usage: ./runner.sh 1 | tee your_output_file"
    echo "Possible output file: $outfile"
    exit 0
fi

make clean && make

nprocs=(1 2 4 8 12 16 24)
hw_args=("kdk.pgm" "netherlands_small.pgm" "netherlands_big.pgm")

# remove previous outputs (clumsy...)
found=0
for a in $(find *.pgm -maxdepth 0); do
    for hw in ${hw_args[@]}; do
        [ "$a" = "$hw" ] && found=1 && break
    done
    [ "$found" -eq 0 ] && rm -f $a; found=0
done

# run experiments
for p in ${hw_args[@]}; do
    for el in ${nprocs[@]}; do
        make run np=$el args="$p $(basename $p .pgm)_$el.pgm"
        echo ...........................
    done
done
