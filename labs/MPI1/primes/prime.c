// File: prime.c
//
// A simple program for computing the amount of prime numbers within
// the interval [a,b].

#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#include "../../../lib/mpi_utils.h"

#define FALSE 0
#define TRUE 1

typedef struct StrategyA {
  uint lb, ub, intervalLen;
  int evenIntervals;
} StrategyA;

/* DO NOT MODIFY */
static int isPrime(unsigned int p) {
  int i, root;
  if (p == 1)
    return FALSE;
  if (p == 2)
    return TRUE;
  if (p % 2 == 0)
    return FALSE;
  root = (int)(1 + sqrt(p));
  for (i = 3; (i < root) && (p % i != 0); i += 2)
    ;
  return i < root ? FALSE : TRUE;
}

void primesStrategyA(StrategyA s, uint *reduceBuff) {
  /* compute start and end of work interval for each thread */
  uint myStart = s.lb + (WHOAMI * s.intervalLen);
  uint myEnd = myStart + s.intervalLen;

  /* depending on interval and number of threads, a worker may start
   * its segment of work on an even number, so make it traverse only
   * odd numbers */
  myStart += (myStart % 2 == 0);

  /* when total work range is not evenly divisible by number of
   * workers, force worker with highest rank to complete its segment
   */
  if (WHOAMI + 1 == NPROCS && !s.evenIntervals)
    myEnd = s.ub + 1;

  /* do work */
  uint i, count = 0;
  for (i = myStart; i < myEnd; i += 2)
    count += isPrime(i);

  /* accumulate partial results into root buffer */
  MPI_Reduce(&count, reduceBuff, 1, MPI_UINT32_T, MPI_SUM, 0,
             MPI_COMM_WORLD);
}

void primesStrategyB() {}

/* TODO strategy B */
/* TODO read strategy from cl */
int main(int argc, char **argv) {
  setbuf(stdout, NULL);

  uint a, b, intervalLen, totCount;
  int evenIntervals, fill2 = FALSE;

  initMPI();

  if (WHOAMI == 0) {
    // Prints current date and user. DO NOT MODIFY
    system("date");
    system("echo $USER");
    printf("\n");

    do {
      printf("Enter two integer number a, b such that 1<=a<=b: ");
      scanf("%u %u", &a, &b);
    } while (a <= 0 || b <= 0 || a > b);

    /* start counting from 3 to count odd numbers only, add 1 to
     * `totCount` if a < 3 */
    fill2 = a < 3 && a != b;
    uint totInterval = b - a + 1;
    /* thread chunk size */
    intervalLen = (uint)(totInterval / NPROCS);
    evenIntervals = intervalLen * NPROCS == totInterval;
  }

  /* time threads execution after user input is given */
  MPI_Barrier(MPI_COMM_WORLD);
  double start = MPI_Wtime();

  /* communicate info derived from user input to all workers */
  MPI_Bcast(&a, 1, MPI_UINT32_T, 0, MPI_COMM_WORLD);
  MPI_Bcast(&b, 1, MPI_UINT32_T, 0, MPI_COMM_WORLD);
  MPI_Bcast(&intervalLen, 1, MPI_UINT32_T, 0, MPI_COMM_WORLD);
  MPI_Bcast(&evenIntervals, 1, MPI_INT, 0, MPI_COMM_WORLD);

  primesStrategyA((StrategyA){a, b, intervalLen, evenIntervals},
                  &totCount);

  double end = MPI_Wtime() - start;

  if (WHOAMI == 0)
    printf("\n#primes=%u\n", totCount + fill2);

  showTimeInfo(&end);

  MPI_Finalize();

  return EXIT_SUCCESS;
}
