#! /usr/bin/bash

nprocs=(1 2 4 8 12 16 24)
timestamp=$(date --iso-8601=seconds)
outfile=peregrine_$timestamp

# normally, run as ./runner | tee output_file
# redirect stdout to timestamped file if 1 is given as arg
[ "$1" = "1" ] && exec > $outfile 2>&1

make

for el in ${nprocs[@]}; do
    echo 1 10000000 | make run np=$el
    echo ...........................
done
