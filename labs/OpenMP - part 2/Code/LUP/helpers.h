#ifndef HELPERS_H
#define HELPERS_H

#include <stdlib.h>

#define real float

#define ABS(a) ((a) < 0 ? (-(a)) : (a))

void *safeMalloc(size_t n);
real **allocMatrix(size_t height, size_t width);
void freeMatrix(real **mat);
real *allocVector(size_t length);
void freeVector(real *vec);
void showMatrix(size_t n, real **A);
void showVector(size_t n, real *vec);

#endif
