/* File: lup.c
 * Author: Arnold Meijster (a.meijster@rug.nl)
 * Version: 1.0 (01 March 2008)
 */

#include <omp.h>
#include <stdio.h>
#include <sys/time.h>

#include "helpers.h"

/* forward references */
void decomposeLUP(size_t n, real **A, size_t *P);
void LUPsolve(size_t n, real **LU, size_t *P, real *x, real *b);
void solve(size_t n, real **A, real *x, real *b);
/********************/

double timer(void) {
  struct timeval tm;
  gettimeofday(&tm, NULL);
  return tm.tv_sec + tm.tv_usec / 1000000.0;
}

void showVector1(size_t n, size_t *vec) {
  size_t i;
  for (i = 0; i < n; ++i) {
    printf("%zu ", vec[i]);
  }
  printf("\n");
}

void decomposeLUP(size_t n, real **A, size_t *P) {
  /* computes L, U, P such that A=L*U*P */

  int h, i, j, k, row;
  real pivot, absval, tmp;

#pragma omp parallel for
  for (i = 0; i < n; ++i) {
    P[i] = i;
  }

  /* NOTE k is automatically private: the iteration variable cannot
     be modified in a `for` work-sharing construct */
#pragma omp parallel for private(i)
  for (k = 0; k < n - 1; ++k) {
    row = -1;
    pivot = 0;
    for (i = k; i < n; ++i) {
      absval = (A[i][k] >= 0 ? A[i][k] : -A[i][k]);
      if (absval > pivot) {
        pivot = absval;
        row = i;
      }
    }

    if (row == -1) {
      printf("Singular matrix\n");
      exit(-1);
    }

    /* swap(P[k],P[row]) */
    h = P[k];
    P[k] = P[row];
    P[row] = h;

    /* swap rows */
    for (i = 0; i < n; ++i) {
      tmp = A[k][i];
      A[k][i] = A[row][i];
      A[row][i] = tmp;
    }

    for (i = k + 1; i < n; ++i) {
      A[i][k] /= A[k][k];
      for (j = k + 1; j < n; ++j) {
        A[i][j] -= A[i][k] * A[k][j];
      }
    }
  }
}

void LUPsolve(size_t n, real **LU, size_t *P, real *x, real *b) {
  real *y;
  size_t i, j;

  /* Solve Ly=Pb using forward substitution */
  y = x; /* warning, y is an alias for x! It is safe, though. */
  for (i = 0; i < n; ++i) {
    y[i] = b[P[i]];
    for (j = 0; j < i; ++j) {
      y[i] -= LU[i][j] * y[j];
    }
  }

  /* Solve Ux=y using backward substitution */
  i = n;
  while (i > 0) {
    i--;
    x[i] = y[i];
    for (j = i + 1; j < n; ++j) {
      x[i] -= LU[i][j] * x[j];
    }
    x[i] /= LU[i][i];
  }
}

void solve(size_t n, real **A, real *x, real *b) {
  /* Construct LUP decomposition */
  size_t *P = safeMalloc(n * sizeof(size_t));
  double clock;

  clock = timer();

  decomposeLUP(n, A, P);
  /* Solve by forward and backward substitution */
  LUPsolve(n, A, P, x, b);

  clock = timer() - clock;
  printf("wallclock: %lf seconds\n", clock);

  free(P);
}

real **createMatrix2b(size_t n) {
  real **M = allocMatrix(n, n);

  int i, j;
  for (i = 0; i != n; ++i)
    for (j = 0; j != n; ++j)
      M[i][j] = i == j ? -2 : (j == i - 1 || j == i + 1 ? 1 : 0);

  return M;
}

int main(int argc, char **argv) {
  real **A, *x, *b;

  A = allocMatrix(3, 3);
  x = allocVector(3);
  b = allocVector(3);

  A[0][0] = 0;
  A[0][1] = 2;
  A[0][2] = 3;
  A[1][0] = 3;
  A[1][1] = 0;
  A[1][2] = 1;
  A[2][0] = 6;
  A[2][1] = 2;
  A[2][2] = 8;

  b[0] = 5;
  b[1] = 4;
  b[2] = 16;

  showMatrix(3, A);
  printf("\n");

  printf("#procs: %d\n", omp_get_max_threads());
  solve(3, A, x, b);

  showVector(3, x);

  freeMatrix(A);
  freeVector(x);
  freeVector(b);

  printf("\n");
  system("echo $USER");
  system("date");

  return EXIT_SUCCESS;
}
