#include <stdio.h>

#include "helpers.h"

void *safeMalloc(size_t n) {
  void *ptr;
  ptr = malloc(n);
  if (ptr == NULL) {
    fprintf(stderr, "Error: malloc(%lu) failed\n", n);
    exit(-1);
  }
  return ptr;
}

real **allocMatrix(size_t height, size_t width) {
  real **matrix;
  size_t row;

  matrix = safeMalloc(height * sizeof(real *));
  matrix[0] = safeMalloc(width * height * sizeof(real));
  for (row = 1; row < height; ++row)
    matrix[row] = matrix[row - 1] + width;
  return matrix;
}

void freeMatrix(real **mat) {
  free(mat[0]);
  free(mat);
}

real *allocVector(size_t length) {
  return safeMalloc(length * sizeof(real));
}

void freeVector(real *vec) { free(vec); }

void showMatrix(size_t n, real **A) {
  size_t i, j;
  for (i = 0; i < n; ++i) {
    for (j = 0; j < n; ++j) {
      printf("%f ", A[i][j]);
    }
    printf("\n");
  }
}

void showVector(size_t n, real *vec) {
  size_t i;
  for (i = 0; i < n; ++i) {
    printf("%f ", vec[i]);
  }
  printf("\n");
}
