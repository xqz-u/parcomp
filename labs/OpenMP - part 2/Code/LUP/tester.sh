#! /usr/bin/zsh

[ "$1" = "" ] && nprocs=1 || nprocs="$1"

export OMP_NUM_THREADS="$nprocs"

make clean
make
make run
