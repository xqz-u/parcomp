#include <math.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define f(x) (4.0 / (1.0 + (x) * (x)))

static double timer(void) {
  struct timeval tm;
  gettimeofday(&tm, NULL);
  return tm.tv_sec + tm.tv_usec / 1000000.0;
}

int numerical_pi(unsigned long N) {
  printf("N=%lu\n", N);

  /* check for exit condition. */
  if (N == 0) {
    printf("Done\n");
    return 1;
  }

  unsigned long i;
  double dx, sum, err;
  double clock;
  /* start timer */
  clock = timer();

  /* numerical integration */
  dx = 1.0 / N;
  sum = 0.0;

#pragma omp parallel for reduction(+ : sum)
  for (i = 0; i != N; ++i) {
    sum += f(dx * (i + 0.5));
  }
  sum *= dx;

  /* stop timer */
  clock = timer() - clock;
  printf("wallclock: %lf seconds\n", clock);

  /* print the results */
  err = sum - 4 * atan(1);
  printf("%lu: pi~%20.18lf   er~=%20.18lf\n", N, sum, err);
  return 0;
}

void integrate_interactive(void) {
  unsigned long N;

  while (1) {
    /* get a value for N */
    do {
      printf(
          "Enter number of approximation intervals:(0 to exit)\n");
      scanf("%lu", &N);
    } while (N < 0);

    if (numerical_pi(N))
      return;
  }
}

int main(int argc, char **argv) {
  setbuf(stdout, NULL);
  printf("omp_get_max_threads(): %d\n", omp_get_max_threads());

  if (argc > 1) {
    char *str_part;
    numerical_pi(strtoul(argv[1], &str_part, 10));
  } else
    integrate_interactive();

  system("echo $USER");
  system("date");

  return EXIT_SUCCESS;
}
