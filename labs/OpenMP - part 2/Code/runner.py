#! /usr/bin/env python

import sys
import json
import subprocess as sp
import os
from pathlib import Path


def file_dir(path: str) -> str:
    return "/".join(path.split("/")[:-1])


PROG_DIR = file_dir(os.path.abspath(__file__))


def module_path(p: str):
    return os.path.join(PROG_DIR, p)


def write_now(fd, s, *args):
    fd.write(s.format(*args))
    fd.flush()


def compile_omp():
    if sorted(Path(PROG_DIR).glob("[mM]akefile")):
        print(f"Compile: make -C {PROG_DIR}")
        ret = sp.Popen(["make", "-C", PROG_DIR]).wait()
    else:
        gcc_compile = "gcc -Wall -g -fopenmp -o"
        c_sources = [module_path(src) for src in Path(PROG_DIR).rglob("*.[cC]")]
        out_exec = module_path("a.out")
        print(f"Compile: {gcc_compile} {out_exec} ", end="")
        print(*c_sources)
        ret = sp.Popen(gcc_compile.split() + [out_exec] + c_sources).wait()
    print(f"Return code: {ret}")


# NOTE runs sequentially if ordered output to a single file is desired :(
def run(specs: dict):
    outfile = (
        module_path(specs["outfile"]) if specs.get("outfile") else sys.stdout.fileno()
    )
    executable = module_path("a.out")
    with open(outfile, "w") as fd:
        for arg in specs["args"]:
            for nproc in specs["procs"]:
                print(f"{executable} {arg} (#proc: {nproc})", end="")
                ret = sp.Popen(
                    [f"{executable}", f"{arg}"],
                    env={**os.environ, "OMP_NUM_THREADS": f"{nproc}"},
                    stdout=fd,
                ).wait()
                write_now(fd, "\n")
                print(f" code: {ret}")
            write_now(fd, "================\n")


def validate_params(d: dict):
    for k, v in d.items():
        assert k in ["args", "procs", "outfile"], f"Unknown key: {k}"
        err = f"Value: {v} for key: {k} is not a"
        if k == "outfile":
            assert isinstance(v, str), f"{err} string"
        else:
            assert isinstance(v, list), f"{err} list"


def read_json(fname: str) -> dict:
    try:
        with open(fname, "r") as fd:
            json_specs = json.load(fd)
    except Exception as ex:
        sys.exit(ex)
    return json_specs


def main(args: list):
    if len(args) > 1:
        prog = args[1]
        global PROG_DIR
        PROG_DIR = module_path(file_dir(prog))
        print(f"Working folder: {PROG_DIR}")
        specs_dict = read_json(prog)
    else:
        specs_dict = read_json("runs.json")
    print(f"Config: {specs_dict}")
    validate_params(specs_dict)
    compile_omp()
    run(specs_dict)


if __name__ == "__main__":
    main(sys.argv)
