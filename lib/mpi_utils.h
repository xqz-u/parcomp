#ifndef MPI_UTILS_H
#define MPI_UTILS_H

typedef struct Benchmark {
  double minT, maxT, avgT;
} Benchmark;

typedef struct MemDivMPI {
  int *chunkSizes, *offsets, mySize;
} MemDivMPI;

/* store information from MPI_Comm_size and MPI_Comm_rank */
extern int NPROCS, WHOAMI;

#define uint unsigned int
#define GENERAL_TAG 99
#define IS_ROOT (WHOAMI == 0)

/* NOTE must be called inside main function, assumes global NPROCS
 * and WHOAMI */
#define initMPI()                                                   \
  MPI_Init(&argc, &argv);                                           \
  MPI_Comm_size(MPI_COMM_WORLD, &NPROCS);                           \
  MPI_Comm_rank(MPI_COMM_WORLD, &WHOAMI)

double startTimer(void);
double stopTimer(double start);
Benchmark timeExecution(double *endT);
void showTimeInfo(double *endT);
MemDivMPI MemDiv(int *chunksInfo, int *offs);
void freeMemDivMPI(MemDivMPI m);
MemDivMPI computePartitionsMPI(int area);
int *distributeIntArr(int *store, MemDivMPI partition);
void collectIntArr(int *sendBuffer, int *recvBuffer,
                   MemDivMPI partition);

#endif
