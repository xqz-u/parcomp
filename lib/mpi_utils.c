#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#include "mpi_utils.h"

int NPROCS, WHOAMI;

double startTimer(void) {
  MPI_Barrier(MPI_COMM_WORLD);
  return MPI_Wtime();
}

double stopTimer(double start) { return MPI_Wtime() - start; }

Benchmark timeExecution(double *endT) {
  Benchmark b;
  MPI_Reduce(endT, &b.minT, 1, MPI_DOUBLE, MPI_MIN, 0,
             MPI_COMM_WORLD);
  MPI_Reduce(endT, &b.maxT, 1, MPI_DOUBLE, MPI_MAX, 0,
             MPI_COMM_WORLD);
  MPI_Reduce(endT, &b.avgT, 1, MPI_DOUBLE, MPI_SUM, 0,
             MPI_COMM_WORLD);
  b.avgT /= NPROCS;
  return b;
}

/* show thread execution time in order, along with min, max and mean
 * execution time */
void showTimeInfo(double *endT) {
  Benchmark bench = timeExecution(endT);
  if (WHOAMI == 0) {
    double r;
    MPI_Status stat;
    printf("\nthread #%d work time: %lfs\n", WHOAMI, *endT);
    for (int i = 1; i != NPROCS; ++i) {
      MPI_Recv(&r, 1, MPI_DOUBLE, i, GENERAL_TAG, MPI_COMM_WORLD,
               &stat);
      printf("thread #%d work time: %lfs\n", i, r);
    }
    printf("Min: %lfs, Max: %lfs, Avg: %lfs\n", bench.minT,
           bench.maxT, bench.avgT);
  } else {
    MPI_Send(endT, 1, MPI_DOUBLE, 0, GENERAL_TAG, MPI_COMM_WORLD);
  }
}

/* NOTE `chunksInfo` and `offs` should already contain information
 on the partitioning of the target memory location*/
MemDivMPI MemDiv(int *chunksInfo, int *offs) {
  return (MemDivMPI){.chunkSizes = chunksInfo,
                     .offsets = offs,
                     .mySize = chunksInfo[WHOAMI]};
}

void freeMemDivMPI(MemDivMPI m) {
  free(m.chunkSizes);
  free(m.offsets);
}

/* useful for MPI_Scatterv and MPI_Gatherv, assumes element of
 * partitioned memory area are contiguous with no space in between */
MemDivMPI computePartitionsMPI(int area) {
  int *chunkSizes = malloc(NPROCS * sizeof(int));
  int *offsets = malloc(NPROCS * sizeof(int));

  int i, chunkSize = area / NPROCS;
  for (i = 0; i != NPROCS - 1; ++i) {
    chunkSizes[i] = chunkSize;
    offsets[i] = i * chunkSize;
  }
  offsets[i] = i * chunkSize;
  /* pad last thread with more work if memory area is not evenly
   * divisible by number of threads */
  chunkSizes[i] = chunkSize + (area % NPROCS);

  return MemDiv(chunkSizes, offsets);
}

/* NOTE generalizable!!! */
int *distributeIntArr(int *store, MemDivMPI partition) {
  int *recvBuff = malloc(partition.mySize * sizeof(int));
  MPI_Scatterv(store, partition.chunkSizes, partition.offsets,
               MPI_INT, recvBuff, partition.mySize, MPI_INT, 0,
               MPI_COMM_WORLD);
  return recvBuff;
}

/* NOTE generalizable!!! */
void collectIntArr(int *sendBuffer, int *recvBuffer,
                   MemDivMPI partition) {
  MPI_Gatherv(sendBuffer, partition.mySize, MPI_INT, recvBuffer,
              partition.chunkSizes, partition.offsets, MPI_INT, 0,
              MPI_COMM_WORLD);
}
